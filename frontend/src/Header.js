// como iremos usar JSX temos que importar React
import React from 'react';

/*
    por padrão as propriedades são passadas como parâmetros na função
    de um componente, por isso vc pode colocar props para pegar todas as propriedades ou
    usar desestruturação para pegar somente propriedades específicas
*/

// function Header(props) {
//     return (
//         <header>
//             {/* <h1>{ props.title }</h1> */}
//             <h1>{ props.children }</h1>
//         </header>
//     );
// }

function Header({ children }) {
    return (
        <header>
            <h1>{ children }</h1>
        </header>
    );
}

export default Header;