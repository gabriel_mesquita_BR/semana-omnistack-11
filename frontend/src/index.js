import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// colocamos o que o componente App retorna no elemento que possui id root
ReactDOM.render(<App />, document.getElementById('root'));
