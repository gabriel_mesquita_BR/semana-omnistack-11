import React, { useState } from 'react';

import { Link, useHistory } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi';

import api from '../../services/api';

import './styles.css';
import logoImg from '../../assets/logo.svg';

export default function Register() {

    /*
        utilizaremos estado para armazenar os dados do formulário

        como o input do nome da ong é texto, inicializamos como uma string vazia o state (estado)
    */
   const [name, setName]         = useState('');
   const [email, setEmail]       = useState('');
   const [whatsapp, setWhatsapp] = useState('');
   const [city, setCity]         = useState('');
   const [uf, setUf]             = useState('');

   /*
       useHistory servirá para redirecionar o usuário para uma rota 
   */
   const history                 = useHistory();

    /*
        por padrão ao submeter um formulário e disparar uma função, o parâmetro desta
        função armazena o evento de submit, e para evitar que a página recarregue ao dar
        um submit no form colocamos o event.preventDefault();
    */
    async function handleRegister(event) {
        event.preventDefault();

        const data = {
            name, email, whatsapp, city, uf
        };

        /*
            envia os dados do formulário para a rota http://localhost:3333/ongs que é a rota
            de criação de ongs da nossa api
        */

        try {
            const response = await api.post('ongs', data);
            alert(`Seu ID de acesso: ${response.data.id}`);
            history.push('/');
        }catch(error) {
            alert('Erro no cadastro, tente novamente');
        }
    }

    return (
        <div className="register-container">
            <div className="content">
                <section>
                    <img src={logoImg} alt="Be The Hero"/>
                    
                    <h1>Cadastro</h1>
                    <p>
                        Faça seu cadastro, entre na plataforma e ajude pessoas a encontrarem os 
                        casos da sua ONG.
                    </p>

                    <Link className="back-link" to="/">
                        <FiArrowLeft size={16} color="#E02041"/>
                        Não tenho cadastro
                    </Link>
                </section>

                <form onSubmit={handleRegister}>
                    {/* event.target.value pega o valor colocado no input e armazena em setName 
                        colocando name entre chaves estou dizendo que é um código javascript
                    */}
                    <input placeholder="Nome da ONG" value={name}
                        onChange={event => setName(event.target.value)}/>

                    <input type="email" placeholder="E-mail" value={email}
                        onChange={event => setEmail(event.target.value)}/>

                    <input placeholder="Whatsapp" value={whatsapp}
                        onChange={event => setWhatsapp(event.target.value)}/>

                    <div className="input-group">
                        <input placeholder="Cidade" value={city}
                        onChange={event => setCity(event.target.value)}/>

                        <input placeholder="UF" style={{ width: 80 }} value={uf}
                            onChange={event => setUf(event.target.value)}/>
                    </div>

                    <button className="button" type="submit">Cadastrar</button>
                </form>
            </div>
        </div>
    );
}