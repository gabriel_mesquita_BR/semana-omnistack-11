import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { FiPower, FiTrash2 } from 'react-icons/fi';
import logoImg from '../../assets/logo.svg';

import api from '../../services/api';

import './styles.css';

export default function Profile() {

    /*
        inicializo o estado como array vazio, pois irá armazenar uma lista de resultados
    */

    const [incidents, setIncidents] = useState([]);

    const history                   = useHistory();

    const ongId                     = localStorage.getItem('ongId');
    const ongName                   = localStorage.getItem('ongName');

    /*
        iremos usar o useEffect para renderizar algo assim que o componente for carregado

        useEffect recebe 2 parâmetros, o primeiro é a função que irá listar os incidentes 
        qnd o componente Profile for carregado e o segundo é qnd essa função será executada

        deixando o array vazio do segundo parâmetro a função do primeiro parâmetro só será
        executada uma única vez, agr se colocássemos ongName no array, toda vez que o valor
        de ongName mudasse a função do primeiro parâmetro seria executada

        OBS: colocamos ongId dentro do array, pois qnd usamos uma constante dentro do useEffect,
             eh interessante colocá-la dentro do array, para caso o id da ong mudasse, fosse
             recalculado a chamada da rota profile da nossa api, claro que o id da ong não irá mudar
    */

    useEffect(() => {
        api.get('profile', {
            headers: {
                Authorization: ongId
            }
        }).then(response => {
            setIncidents(response.data.incidents);
        })
    }, [ongId]);

    async function handleDeleteIncident(id) {

        try {
            await api.delete(`incidents/${id}`, {
                headers: {
                    Authorization: ongId
                }
            });

            /*
                uso o filter para setIncidents deixar de armazenar o incidente deletado e assim
                o caso sumir da tela ao ser deletado
            */
            setIncidents(incidents.filter(incident => incident.id !== id));

        }catch(error) {
            alert('Erro ao deletar o caso, tente novamente');
        }
    }

    function handleLogout() {
        localStorage.clear();
        history.push('/');
    }

    return (
        <div className="profile-container">
            <header>
                <img src={logoImg} alt="Be The Hero"/>
                <span>Bem vinda, { ongName }</span>

                <Link className="button" to="/incidents/new">Cadastrar novo caso</Link>
                <button onClick={handleLogout} type="button">
                    <FiPower size={18} color="#E02041"/>
                </button>
            </header>

            <h1>Casos cadastrados</h1>
        
            <ul>
                {/* depois da arrow function coloco parênteses, pois assim o JSX que eu colocar
                    dentro do parênteses já eh retornado, se eu colocasse chaves aí pra retornar
                    o JSX teria que usar o return

                    qnd se usa um loop seja map, foreach, no react é interessante no primeiro
                    elemento que vem depois da propriedade de loop colocar key, para que seja
                    possível identificar cada elemento
                */}
                { incidents.map(incident => (
                    <li key={ incident.id }>
                        <strong>CASO:</strong>
                        <p>{ incident.title }</p>

                        <strong>DESCRIÇÃO:</strong>
                        <p>{ incident.description }</p>

                        <strong>VALOR:</strong>
                        <p>
                            { Intl.NumberFormat('pt-BR', 
                                { style: 'currency', currency: 'BRL'}).format(incident.value) }
                        </p>

                        {/* se eu colocar assim onClick={handleDeleteIncident(incident.id)} 
                            qnd o componente Profile for carregado todos os incidentes 
                            serão deletados, pois colocando dessa forma estou executando a função

                            acrescentando uma arrow function no onClick faço com que esse problema
                            suma, pois agora estou passando para o onClick uma função e não o
                            retorno de uma função
                        */}

                        <button onClick={() => handleDeleteIncident(incident.id)} type="button">
                            <FiTrash2 size={20} color="#a8a8b3"/>
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
}