import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Logon from './pages/Logon';
import Register from './pages/Register';
import Profile from './pages/Profile';
import NewIncident from './pages/NewIncident';

export default function Routes() {

    /*
        Switch garante que só seja chamado 1 rota
    */

    return (
        <BrowserRouter>
            <Switch>

                {/* no react ao passar uma rota na barra de endereços, ele não vai verificar se
                    essa rota é igual a rota da propriedade path, o react vai apenas verificar se
                    começa com "/", e como a página de Logon também começa com "/" não importa
                    se vc colocar /register, pois sempre vai cair na rota de logon
                    
                    para consertar isso adicionamos na rota de Logon a propriedade exact, pois assim
                    estamos dizendo para o react que para acessar a página de Logon a rota tem que
                    ser exatamente apenas "/" 
                */}

                <Route path="/" exact component={Logon}/>
                <Route path="/register" component={Register}/>
                <Route path="/profile" component={Profile}/>
                <Route path="/incidents/new" component={NewIncident}/>
            </Switch>
        </BrowserRouter>
    );
}