// import React, { useState } from 'react';

import React from 'react';

import './global.css';

// n precisamos colocar o arquivo (index.js), pois ao importarmos uma página automaticamente
// procura-se pelo arquivo index.js
// import Logon from './pages/Logon';

import Routes from './routes';

// import Header from './Header';

/*
  App é um componente
  um componente nada mais é que uma função que retorna html
  qnd temos um html dentro do javascript chamamos ele de JSX (JAVASCRIOT XML)
  XML é a sintaxe do HTML

  title é uma propriedade do componente Header
*/
function App() {

  /*
    utilizaremos estado para ao ir clicando no botão o componente ir renderizando e o valor
    incrementando na tela

    useState(0) faz o counter ser igual a 0

    useState nos retorna um array [valor_do_estado, funcao_de_atualização_deste_valor]

  */

  // const [counter, setCounter] = useState(0);

  // function increment() {
    /*
      nós não podemos manipular o valor de uma variável de estado de forma direta, por causa do 
      conceito de imutabilidade do React
    */
    // counter++;

   // setCounter(counter + 1);
  // }

  return (

    <Routes/>

    // <Logon/>

    // <Header title="Semana Omnistack"/>

    // <Header>
    //   Semana Omnistack
    // </Header>

    // <div>
    //   <Header>Contador: { counter }</Header>
    //   <button onClick={ increment }>Incrementar</button>
    // </div>
  );
}

export default App;
