const express                      = require('express');
const { celebrate, Segments, Joi } = require('celebrate');

const OngsController               = require('./controllers/OngsController');
const IncidentsController          = require('./controllers/IncidentsController');
const ProfilesController           = require('./controllers/ProfilesController');
const SessionController            = require('./controllers/SessionController');

// const crypto         = require('crypto');
// const connection     = require('./database/connection');

// jogo a rota do express em uma variável, desacoplando a rota do express
const routes  = express.Router();

// routes.get('/ongs', async (request, response) => {
//     // pego todos os dados da tabela ongs
//     const ongs = await connection('ongs').select('*');
//     return response.json(ongs);
// });

routes.post('/sessions', SessionController.create);

routes.get('/ongs', OngsController.index);

/*
    precisamos passar o celebrate() antes de OngsController.create para que a validação ocorra antes
    da criação da ong
*/

routes.post('/ongs', celebrate({
    /*
        aqui nós definimos o que queremos validar, se eh os query params, os route params, body,
        headers,...

        na criação da ong vamos validar somente o body, pois não temos os outros

        pra validar o body temos que usar o Segments.Body como chave já q estamos dentro de um objeto,
        porém Segments.Body é um código javascript, por isso pra ser reconhecido pelo react temos
        q colocar entre colchetes já que estamos dentro de um objeto
    */

    [Segments.BODY]: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        whatsapp: Joi.string().required().min(10).max(11),
        city: Joi.string().required(),
        uf: Joi.string().required().length(2)
    })

}), OngsController.create);

routes.get('/incidents', celebrate({
    // valida os query params
    [Segments.QUERY]: Joi.object().keys({
        page: Joi.number()
    })
}), IncidentsController.index);


routes.post('/incidents', IncidentsController.create);

routes.delete('/incidents/:id', celebrate({
    // valida os route params
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.number().required()
    })
}), IncidentsController.delete);

routes.get('/profile', celebrate({

    /*
        valido os headers
        
        coloco unknown para que as propriedades que são passadas pelo header e que eu não conheço,
        o Joi não tente validá-las
    */

    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown()
}), ProfilesController.index);

// exportamos a variável routes para poder ser chamada em qualquer parte do projeto
module.exports = routes;