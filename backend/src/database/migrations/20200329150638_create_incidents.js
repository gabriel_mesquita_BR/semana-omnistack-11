
exports.up = function(knex) {
    return knex.schema.createTable('incidents', function(table) {
        // a chave primária irá incrementando automaticamente a cada inserção
        table.increments();
        table.string('title').notNullable();
        table.string('description').notNullable();
        table.decimal('value').notNullable();

        // o id da ong que criou o incidente, logo teremos uma chave estrangeira
        table.string('ong_id').notNullable();
        table.foreign('ong_id').references('id').inTable('ongs');
    });
};

exports.down = function(knex) {
  return knex.schema.dropTable('incidents');
};
