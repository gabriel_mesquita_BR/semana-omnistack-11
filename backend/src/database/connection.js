const knex          = require('knex');
const configuration = require('../../knexfile'); 

// pega o valor da variável de ambiente NODE_ENV
const config        = process.env.NODE_ENV === 'test' ? configuration.test : configuration.development;

// estabeleço a conexão com o bd no ambiente development
// const connection    = knex(configuration.development);

const connection    = knex(config);

module.exports      = connection;