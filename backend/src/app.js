/* Arquivo Principal */

/*
    BANCO DE DADOS

    SQL  : MYSQL, SQLITE, POSTGRES, SQL SERVER => Banco de dados relacional
    NOSQL: MONGODB, COUCHDB,....               => Banco de dados não relacional

*/

// importo o módulo express
const express    = require('express');

const cors       = require('cors');

const { errors } = require('celebrate');

// preciso usar o ./ porque se não o node irá achar q routes eh um pacote, sendo que ele é um arquivo
const routes     = require('./routes');

// armazena a aplicação
const app        = express();

/*
    se a nossa aplicação estivesse em produção acrescentaria em cors o origin, porque assim
    restrinjo quem pode acessar a nossa api, mas como estamos em desenvolvimento pode-se ignorar isso
*/

// app.use(cors({
//     'origin': 'http://meuapp.com'
// }));

app.use(cors());

/*
    faço isso para dizer ao express que ao buscar dados do corpo, payload de uma requisição estaremos
    esperando os dados no formato json, porque se não fizéssemos isso ao colocar o comando:

    request.body;

    o retorno dele seria undefined, o express não conseguiria reconhecer como json
*/
app.use(express.json());

// dessa forma routes estará sendo usada na aplicação e tem q estar abaixo da linha acima
app.use(routes);

app.use(errors());

// // crio a rota do tipo get
// app.get('/', (request, response) => {
//     // return response.send('Hello World');
//     return response.json({
//         evento : 'Semana OmniStack 11'
//     });
// });

// app.get('/users', (request, response) => {

//      // pega a chave e valor dos query params enviados pela uri
//      const params = request.query;
//      console.log(params);

//      return response.json({
//         evento : 'Semana OmniStack 11'
//     });
// });

// app.get('/users/:id', (request, response) => {

//     // pego o id enviado pela requisição
//     const id = request.params;
//     console.log(id);

//     return response.json({
//        evento : 'Semana OmniStack 11'
//    });
// });

// app.post('/users', (request, response) => {

//     // pego os dados que normalmente é no formato json enviado na requisição através do body, payload
//     const body = request.body;
//     console.log(body);

//     return response.json({
//        evento : 'Semana OmniStack 11'
//    });
// });

// faço a minha aplicação ser ouvida na porta 3333
// app.listen(3333);

module.exports = app;