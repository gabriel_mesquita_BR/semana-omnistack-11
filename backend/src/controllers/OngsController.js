// const crypto     = require('crypto');
const generateUniqueId = require('../utils/generateUniqueId');
const connection       = require('../database/connection');

module.exports = {
    
    async index(request, response) {
        // pego todos os dados da tabela ongs
        const ongs = await connection('ongs').select('*');
        return response.json(ongs);
    },

    async create(request, response) {
        // faço a desestruturação para armazenar cada dado em uma constante
        const { name, email, whatsapp, city, uf } = request.body;
        
        /*
            nas ongs os ids não serão gerados automaticamente, como nos incidents, por isso iremos
            usar a biblioteca crypto que já vem com o node para criarmos esse id de forms aleatório
            e no formato de string

            gera 4 bytes de caracteres hexadecimais
        */

        // const id = crypto.randomBytes(4).toString('HEX');

        const id = generateUniqueId();

        // coloco a tabela que eu quero inserir os dados e passo os nomes das colunas
        // como essa inserção pode demorar e só queremos retornar o json após a inserção, iremos
        // utilizar async e await
        await connection('ongs').insert({
            id,
            name,
            email,
            whatsapp,
            city,
            uf
        });

        return response.json({ id });
    },
}