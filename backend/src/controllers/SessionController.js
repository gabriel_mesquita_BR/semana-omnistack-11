// este controller refere-se ao Login da Ong => ao realizar um login é criado uma sessão e ao 
// realizar logout a sessão é deletada

const connection = require('../database/connection');

module.exports = {
    async create(request, response) {
        // pego o id da ong que vem pelo corpo da requisição
        const { id } = request.body;

        const ong    = await connection('ongs')
            .where('id', id)
            .select('name')
            .first();

        if(!ong) {
            return response.status(400).json({ error: 'No ONG found with this ID' })
        }

        return response.json(ong);
    }
}

