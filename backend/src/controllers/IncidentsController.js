const connection = require('../database/connection');

module.exports = {

    async index(request, response) {

        /*
            n queremos mostrar todos os dados de uma só vez, por isso iremos utilizar
            paginação

            uso request.query para pegar os query params, que será a paginação
            através da desestruturação pego a chave page, se a chave page não existir, coloco
            1 como valor padrão

            coloco limit como 5 para pegar 5 incidentes
            
            offset coloco (page - 1) * 5, pq qnd page for igual a 1 o offset será 0, assim serão
            pegos os 5 primeiros incidentes, qnd page for igual a 2 o offset será 5, assim os 5
            primeiros incidentes serão pulados, e os próximos 5 incidentes que serão pegos são
            os de id 6 até o id 10, e assim por diante...
        */

        const { page = 1 } = request.query;

        /*
            como esse count() retorna um array para pegarmos o valor da sua primeira posição
            q será a quantidade total de incidentes, usamos colchetes, e este valor será armazenado 
            na constante count 
        */
        const [count] = await connection('incidents').count();

        /*
            uso o join para trazer os dados da ong levando em conta que o id da ong deve ser
            igual ao id de ong_id de incidentes

            se eu usar .select('*'); o id do incidente seria sobrescrito pelo id da ong, pois
            o valor do id em ambos é armazenado por uma chave de mesmo nome (id), por isso
            faço da forma abaixo:

            .select(['incidents.*', 'ongs.name', 'ongs.email', 'ongs.whatsapp', 'ongs.city', 
                'ongs.uf']

            assim estou dizendo que quero todos os dados do incidente e alguns dados da ong
        */

        const incidents = await connection('incidents')
            .join('ongs', 'ongs.id', '=', 'incidents.ong_id')
            .limit(5)
            .offset((page - 1) * 5)
            .select(['incidents.*', 'ongs.name', 'ongs.email', 'ongs.whatsapp', 'ongs.city', 
                'ongs.uf']);

        /*
            iremos mandar o total de incidentes não pelo corpo da requisição, mas sim pelo header
            da resposta

            se nós colocarmos somente count ele retorna uma chave e valor:
            'count(*)': número_total_incidentes
        */

        response.header('X-Total-Count', count['count(*)']);
            
        return response.json({ incidents });
    },

    async create(request, response) {
        const { title, description, value } = request.body;
        const ong_id                        = request.headers.authorization;

        /*
            como este insert retorna um array, para pegarmos o valor da sua primeira posição
            q será o id, usamos colchetes, e este valor será armazenado na constante id
        */

        const [id] = await connection('incidents').insert({
            title,
            description,
            value,
            ong_id
        });

        return response.json({ id });
    },

    async delete(request, response) {
        const { id } = request.params;
        const ong_id                        = request.headers.authorization;

        /*
            preciso verificar se o incidente que vai ser deletado pertence a ong que está
            querendo deletá-lo, por isso peguei o id da ong

            coloquei first(), pois sei que só será retornado um único resultado, assim n
            será retornado um array
        */

        const incident = await connection('incidents')
            .where('id', id)
            .select('ong_id')
            .first();

        if(incident.ong_id !== ong_id) {
            return response.status(401).json({ error: 'Operation not permitted' });
        }

        await connection('incidents').where('id', id).delete();

        return response.status(204).send();
    }
}