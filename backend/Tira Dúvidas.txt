npm init -y => inicializa o nosso projeto node js aceitando todas as perguntas q vierem (criando um package.json)

npm install express => instala o micro framework express

node index.js => executo a aplicação

se surgir esse erro: Cannot GET / ao executar a aplicação é por que vc não criou rotas

para utilizar o recurso watch, ou seja, o node ficar sempre monitorando se houve alguma mudança no código e subir o servidor
novamente, sem precisarmos fazer isso manualmente através do comando: node index.js baixaremos o nodemon

npm install nodemon -D

OBS: ao colocar -D estou dizendo para instalar esse recurso somente em ambiente de desenvolvimento, pois não tem sentido em
     colocá-lo em produção

para utilizar o nodemon através do npm, iremos em package.json e na chave scripts criaremos um script:
colocamos a chave "start" e como valor "nodemon index.js", assim ao executar no terminal o comando: npm start a aplicação
é executada com o recurso de watch


iremos utilizar o KNEX.JS que é um query builder, para comunicarmos com o bd e fazermos o processo de inserção, deleção, ..., no bd

npm install knex

e após isso devemos instalar o driver do bd q iremos usar, como neste curso iremos usar o sqlite, colocamos o comando abaixo:
npm install sqlite3

com estes carinhas instalados podemos executar o knex para começarmos a configurar a conexão com o bd:
npx knex init

para criarmos uma migration fazendo com que o knex.js converta a entidade para tabela, ou seja, converta o mundo O.O para o relacional,
temos que fazer os seguintes passos:

criar um diretório chamado migrations no diretório criado e nomeado de database
ir no knexfile.js, dentro da chave development, criar uma chave migration com o valor './src/database/migrations' que é onde
localiza-se o diretório migrations que vc criou

feito isso executaremos o comando abaixo para criar a migration:

npx knex migrate:make create_ongs

ao executar esse comando a migration será criado, porém surgirá um warning dizendo que o sqlite não suporta default values, para 
resolver isso basta ir em knexfile.js e acrescentar a chave useNullAsDefault com valor true na chave development

para executar a migration criada execute: npx knex migrate:latest

para desfazer a última migration executada: npx knex migrate:rollback

para saber as migration que foram executadas: npx knex migrate:status

nesta api, a chave estrangeira ong_id não terá seu valor passado pelo payload, poderia passar, mas como é uma única chave
estrangeira, tendo o id da ong que estará logada na nossa aplicação registrando os incidentes, passaremos o seu valor 
na chave Authorization no cabeçalho da requisição


para evitar erro de cors iremos baixar o pacote de cors: npm install cors

para fazermos as validações dos dados no backend usaremos a biblioteca celebrate: npm install celebrate

o celebrate itegra a biblioteca joi (responsável por fazer de fato as validações) com o framework express (q eh o framework
q estamos usando)

para efetuarmos testes na nossa aplicação iremos usar o framework de testes nomeados de Jest: npm install jest

para executarmos o Jest colocamos: npx jest --init

ele fará 4 perguntas: a primeira eh se queremos adicionar um script pra executar os testes no package.json (digitamos Y)
		      a segunda é se o ambiente de testes será no node ou browser (escolhemos node)
		      a terceira é se queremos que o Jest adicione relatório de cobertura (digitamos N)	
		      a quarta é se queremos que cada coisa que definimos em um teste não seja valido pro outro teste (digitamos Y)


teste de integração verifica uma rota, funcionalidade por completo

teste unitário verifica algo de forma muito isolado, por exemplo uma função que retorna algo muito específico como a que criamos:

export default function generateUniqueId() {
    return crypto.randomBytes(4).toString('HEX');
}

pois a função acima não acessa banco de dados, n chama outras funcionalidades

para fazermos os testes de integração que são mais detalhados, iremos criar um banco de testes para que os dados não sejam
criados no banco de desenvolvimento

no knexfile.js criamos o banco de testes

instalamos o cross-env: npm install cross-env

no package.json na chave test dentro da chave scripts colocamos: "cross-env NODE_ENV=test jest"
ou seja, a nossa chave test tem 2 scripts, sendo que ao colocarmos cross-env NODE_ENV=test estamos dizendo que qnd o script
test for executado estaremos no ambiente de teste

para fazermos os testes de integração, temos que fazer a chamada da nossa api, sendo que estaremos no backend, logo iremos utilizar
uma biblioteca chamada supertest

npm install supertest -D => instala como dependência somente para o ambiente de desenvolvimento


para fazer deploy de aplicações experimentais feitas no node => heroku
para fazer deploy de aplicações comerciais feitas no node => digitalocean

outras alternativas: aws, google cloud platform e o microsoft azure
