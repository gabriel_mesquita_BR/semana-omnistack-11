const request = require('supertest');

/*
    mudamos o nome de index.js para app.js
    no arquivo app.js comentamos a linha responsável por fazer a minha aplicação ser ouvida na
    porta 3333
    e mudamos essa responsabilidade para server.js
    fizemos essas modificações, pois vamos importar o arquivo app.js e se tivessemos mantido
    as configurações anteriores qnd eu executasse o teste a minha aplicação ia ser executada,
    estaria sendo ouvida na porta 3333 e não é isso que queremos
*/

const app        = require('../../src/app');
const connection = require('../../src/database/connection'); 

describe('ONG', () => {

    /*
        para criar as tabelas no banco de testes, quem irá executar as migrations é o próprio
        arquivo de teste

        beforeEach() => antes de cada teste
    */

    beforeEach(async () => {
        /*
            eh interessante antes de executar as migrations dar um rollback para evitar que o
            banco de testes fique muito pesado
        */
        await connection.migrate.rollback();
        await connection.migrate.latest();
    });

    /*
        para evitar esse warning:

        A worker process has failed to exit gracefully and has been force exited. This is likely 
        caused by tests leaking due to improper teardown. Try running with --runInBand 
        --detectOpenHandles to find leaks.

        temos que destruir a conexão com o bd de testes, pois foi ela que causou esse warning,
        porque ficou aberta após o fim dos testes

        após todos os testes destrói a conexão com o bd de testes
    */

    afterAll(async () => {
        await connection.destroy();
    });

    it('should be able to create a new ONG', async () => {

        /*
            se essa rota fosse necessário passar um header bastava colocar antes de send,
            set('Authorization', 'id_da_ong')
        */

        const response = await request(app).post('/ongs').send({
            name: "APAD2",
            email: "contato@contato.com",
            whatsapp: "4700000000",
            city: "Rio do Sul",
            uf: "SC"
        });

        expect(response.body).toHaveProperty('id');
        expect(response.body.id).toHaveLength(8);
    });
})