const generateUniqueId = require('../../src/utils/generateUniqueId');

/*
    primeiro parâmetro definimos o nome do que iremos testar
*/
describe('Generate Unique Id', () => {
    /*
        it é do inglês isto
        logo abaixo vc diz: isto deve gerar um id único
    */

    it('should generate unique ID', () => {
        // aqui efetua os testes

        const id = generateUniqueId();
        expect(id).toHaveLength(8);
    })
});